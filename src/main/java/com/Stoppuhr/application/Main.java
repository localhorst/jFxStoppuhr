package main.java.com.Stoppuhr.application;


import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.Animation.Status;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;

/**
 * Praktikum OOSE SS 2018 Aufgabenblatt 3, Aufgabe 4
 * 
 * @author [Hendrik Schutter]
 */

public class Main extends Application
{

	@FXML
	private Label zeitAnzeige;

	@FXML
	private AnchorPane mainAnchorpane;

	@FXML
	private MenuItem menuItemStart;

	@FXML
	private MenuItem menuItemReset;

	@FXML
	private MenuItem menuItemClose;

	@FXML
	private MenuItem menuItemHilfe;

	@FXML
	private Button startStopButton;

	@FXML
	private Button resetButton;

	private static Timeline timeline;

	private int min = 0;

	private int sec = 0;

	private int mil = 0;

	private static boolean timer = true;

	@Override
	public void start(Stage primaryStage)
	{
		try {
			FXMLLoader loader = new FXMLLoader();
			//TODO Path
			loader.setLocation(Main.class.getResource("../../../../resources/fxml/MainWindow.fxml"));
			//System.out.println(Main.class.getResource("../../../../resources/fxml/MainWindow.fxml").toString());
			//System.exit(1);
			
			AnchorPane pane = loader.load();
			primaryStage.setTitle("Stoppuhr");
			Scene scene = new Scene(pane);
			/**
			 * alle event listener mit fxml-loader
			 *
			 */
			scene.getStylesheets().add(
					Main.class.getResource("../../../../resources/css/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void menuItemStartAction(ActionEvent event)
	{
		startOrStopCounting();
	}

	@FXML
	public void menuItemResetAction(ActionEvent event)
	{
		resetTime();
		updateTimeLabel();
	}

	@FXML
	public void menuItemCloseAction(ActionEvent event)
	{
		System.exit(0);
	}

	@FXML
	public void menuItemHilfeAction(ActionEvent event)
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Information");
		alert.setHeaderText("Praktikum OOSE SS 2018 Aufgabenblatt 3, Aufgabe 4");
		alert.setContentText("Author: Hendrik Schutter - 183129");
		alert.showAndWait();
	}

	@FXML
	public void startStopButtonAction(ActionEvent event)
	{
		startOrStopCounting();
	}

	@FXML
	public void resetButtonAction(ActionEvent event)
	{
		resetTime();
		updateTimeLabel();
	}

	public static void main(String[] args)
	{
		launch(args);
	}

	/**
	 * Aktualisiert die Zeitanzeige.
	 */
	private void updateTimeLabel()
	{
		if (mil > 99) {
			sec = sec + 1;
			mil = 0;
		}

		if (sec > 59) {
			min = min + 1;
			sec = 0;
		}

		String timeMin = Integer.toString(min);
		String timeSec = Integer.toString(sec);
		String timeMil = Integer.toString(mil);

		if (mil < 10) {
			timeMil = "0" + Integer.toString(mil);
		}

		if (sec < 10) {
			timeSec = "0" + Integer.toString(sec);
		}

		if (min < 10) {
			timeMin = "0" + Integer.toString(min);
		}

		String time = timeMin + ":" + timeSec + ":" + timeMil;

		zeitAnzeige.setText(time);

		// System.out.println("Die Zeit: " + time);

		// Text im Label neu setzen
	}

	/**
	 * Diese Methode startet bzw. stoppt die Uhr. Sie soll beim Drücken des
	 * Start/Stop-Buttons aufgerufen werden.
	 */
	private void startOrStopCounting()
	{
		// Je nachdem, ob die Uhr gerade läuft oder nicht:
		// Timer pausieren oder starten, dann

		if (timer) {
			timeline = new Timeline(new KeyFrame(Duration.millis(10), event -> {
				incrementTime(); // update time
			}));
			timeline.setCycleCount(Animation.INDEFINITE);
			timer = false;
		}
		if (timeline.getStatus() == Status.STOPPED
				|| timeline.getStatus() == Status.PAUSED) {
			timeline.play();
			startStopButton.setText("Stop");
			menuItemStart.setText("Stop");
		} else {
			timeline.stop();
			startStopButton.setText("Start");
			menuItemStart.setText("Start");
		}

	}

	/**
	 * Diese Methode setzt die Uhr auf 00:00:00 zurück. Sie soll beim Drücken des
	 * Start/Stop-Buttons aufgerufen werden.
	 */
	private void resetTime()
	{
		min = 0;
		sec = 0;
		mil = 0;
	}

	/**
	 * Diese Methode erhöht die Zeit um eine hunderstel Sekunde. Sie muss vom
	 * Timer (s. Methode 'start') jede hundertstel Sekunde aufgerufen werden.
	 */
	private void incrementTime()
	{
		// Zeitvariable(n) erhöhen
		// und Anzeige aktualisieren

		mil = mil + 1;
		updateTimeLabel();
	}

}

class InnerClassHandler implements EventHandler<ActionEvent>
{
	@Override
	public void handle(ActionEvent event)
	{
		System.out.println("text");
	}
}
